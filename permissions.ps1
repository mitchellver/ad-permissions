# uses PS module powershell-yaml: Install-Module powershell-yaml
# based on http://dbadailystuff.com/yaml-in-powershell
[string[]]$fileContent = Get-Content .\groups.yaml
$content = ''

$lookup = @{}

$lookup["0101"] = "PAWP"
$lookup["0102"] = "Galloper"
$lookup["0104"] = "Nobelwind"
$lookup["0106"] = "Rentel"
$lookup["0107"] = "Norther"
$lookup["0112"] = "KalloSluis"
$lookup["0115"] = "Northwind"
$lookup["0118"] = "SJOR"
$lookup["0121"] = "Belwind"
$lookup["0126"] = "Northwind"

$credential = get-credential

foreach ($line in $fileContent) { $content = $content + "`n" + $line }
$yaml = ConvertFrom-YAML $content

$yaml.GetEnumerator() | Foreach-Object {
	$no_member_of = @()
	$already_member_of = @()

	$user = $_.Name
	# write-host "User: " $user

	$member_of = Get-ADPrincipalGroupMembership $user -Server 24sea.local -Credential $credential | select SamAccountName

	$yaml.$user.GetEnumerator() | Foreach-Object {
		$project_code = $_.Name
		$project_name = $lookup[$project_code]
		# write-host "Project: " $project_code "(" $project_name ")"

		$yaml.$user.$project_code.GetEnumerator() | Foreach-Object {
			$application = $_.Name
			$permission = $yaml.$user.$project_code.$application
			# write-host "Application (permission): " $application "(" $permission ")"

			$ad_group = "{0:d4}-{1}-{2}-{3}" -f $project_code,$project_name,$permission,$application

			if ([bool]($member_of -match $ad_group)) {
				$already_member_of += $ad_group
			}else{
				$no_member_of += $ad_group
			}
		}
	}
	if ($no_member_of) {
		"{0} is not a member of:" -f $user
		$no_member_of
	}
	if ($already_member_of) {
		"And already a member of:"
		$already_member_of
	}
}
